<?php
/**
 * Created by PhpStorm.
 * User: Aleksei
 * Date: 08.03.2017
 * Time: 2:49
 */

namespace ExplorerBundle\Command\SyncCommand;

use ExplorerBundle\Entity\DirEntry;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;


class SyncCommand extends ContainerAwareCommand
{
    private $entityManager;
    private $dirEntryRepository;

    protected function configure()
    {
        $this
            ->setName('explorer:sync')
            ->setDescription('Synchronize folder with database.')
            ->addArgument('path', InputArgument::REQUIRED, "Absolute path to folder for sync with db.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sync_dir = $input->getArgument('path');
        $output->write("Пожалуйста подождите.\nИдет синхронизация содержимого папки " . $sync_dir . " c базой данных... ");
        $this->dirEntryRepository = $repository = $this->getContainer()->get('doctrine')->getRepository('ExplorerBundle:DirEntry');
        $dirEntry = $this->dirEntryRepository->findOneByName(DirEntry::ROOT_SIGNATURE);
        $this->entityManager = $this->getContainer()->get('doctrine')->getManager();
        $this->scanDirectory($dirEntry, $sync_dir, 0);
        $this->entityManager->flush();
        $this->entityManager->clear();
        $output->write("Завершено!\n");
    }

    /**
     * Ищет элемент ФС с указанным именем и типом в массиве.
     * @param $dirEntries
     * @param $name
     * @param $type
     * @return array массив длииной 1 или 0
     */
    private function getDirEntry($dirEntries, $name, $type)
    {
        $result = array();
        if (count($dirEntries) > 0) {
            $result = array_filter(
                $dirEntries,
                function ($e) use ($name, $type) {
                    return strcmp($e->getName(), $name) == 0 and $e->getType() == $type;
                }
            );
        }
        return $result;
    }

    /**
     * Обеспечивает корректную работу с русскими символами в названиях файлов и папок, в Windows.
     * @param $str
     * @return string
     */
    private function toUtf($str)
    {

        return strcmp(PHP_OS, 'WINNT') == 0 ? iconv('cp1251', 'utf-8', $str) : $str;
    }

    /**
     * Возвращает идентификатор типа элемента ФС.
     * @param $file
     * @return int
     */
    private function getDirEntryTypeId($file)
    {
        $result = DirEntry::TYPE_FILE;

        if ($file->isDir()) {
            $result = DirEntry::TYPE_DIRECTORY;
        }

        return $result;
    }

    /**
     * Преобразует timestamp в DateTime с учетом текущей временой зоны.
     * @param $timestamp
     * @return \DateTime
     */
    private function toDateTime($timestamp)
    {
        $date = new \DateTime('@' . $timestamp);
        $date->setTimeZone(new \DateTimeZone(date_default_timezone_get()));
        return $date;
    }

    /**
     * Сохраняет информацию о каталоге в БД и возвращает объект DirEntry.
     * @param $file
     * @param $pathName
     * @param $rootDirEntry
     * @return DirEntry
     */
    private function saveDirectory($file, $pathName, $rootDirEntry)
    {
        $actualDirEntry = new DirEntry();
        $actualDirEntry->setName($pathName);
        $actualDirEntry->setType(DirEntry::TYPE_DIRECTORY);
        $actualDirEntry->setDirectory($rootDirEntry);
        $actualDirEntry->setSize(0);
        $actualDirEntry->setCreated($this->toDateTime($file->getCTime()));
        $actualDirEntry->setModified($this->toDateTime($file->getMTime()));
        $this->entityManager->persist($actualDirEntry);
        return $actualDirEntry;
    }

    /**
     * Сохраняет информацию о файле в БД.
     * @param $file
     * @param $pathName
     * @param $rootDirEntry
     */
    private function saveFile($file, $pathName, $rootDirEntry)
    {
        $actualDirEntry = new DirEntry();
        $guesser = new FileinfoMimeTypeGuesser();
        $actualDirEntry->setName($pathName);
        $actualDirEntry->setType(DirEntry::TYPE_FILE);
        $actualDirEntry->setDirectory($rootDirEntry);
        $actualDirEntry->setSize($file->getSize());
        $actualDirEntry->setMimeType($guesser->guess($file->getRealPath()));
        $actualDirEntry->setCreated($this->toDateTime($file->getCTime()));
        $actualDirEntry->setModified($this->toDateTime($file->getMTime()));
        $this->entityManager->persist($actualDirEntry);
    }

    /**
     * Проверяет наличие изменений в файле и обновляет информацию, если они есть.
     * @param $file
     * @param $actualDirEntry
     */
    private function checkUpdateFile($file, $actualDirEntry)
    {
        if ($actualDirEntry->getSize() != $file->getSize()
            or $actualDirEntry->getModified() != $this->toDateTime($file->getMTime())
        ) {
            $guesser = new FileinfoMimeTypeGuesser();
            $actualDirEntry->setSize($file->getSize());
            $actualDirEntry->setMimeType($guesser->guess($file->getRealPath()));
            $actualDirEntry->setCreated($this->toDateTime($file->getCTime()));
            $actualDirEntry->setModified($this->toDateTime($file->getMTime()));
            $this->entityManager->persist($actualDirEntry);
        }
    }

    /**
     * Рекурсивно обходит каталог и все найденные файлы и папки записывает в БД.
     * @param $rootDirEntry
     * @param $realPath
     * @param $level
     */
    private function scanDirectory($rootDirEntry, $realPath, $level)
    {
        $finder = new Finder();
        $finder->ignoreDotFiles(false);
        $finder->ignoreVCS(false);
        $finder->ignoreUnreadableDirs(false);
        $finder->in($realPath);
        $finder->depth(0);

        // Получаем потомков, для того, чтобы тех, которые имеются в ФС удалять из этого списка
        // а те, что в итоге остались, удалить из БД.
        $childDirEntries = $this->dirEntryRepository->findBy(array('directory' => $rootDirEntry));

        foreach ($finder as $file) {
            $pathName = $this->toUtf($file->getRelativePathname());

            // Получаем информацию о файле из БД
            $actualDirEntry = $this->getDirEntry($childDirEntries, $pathName, $this->getDirEntryTypeId($file));
            // Если запись об этом файле есть
            if (count($actualDirEntry) > 0) {
                // Исключаем его из списка файлов в БД подлежающих удалению.
                $actualDirEntryKey = array_keys($actualDirEntry)[0];
                unset($childDirEntries[$actualDirEntryKey]);
                $actualDirEntry = $actualDirEntry[$actualDirEntryKey];
            } else {
                $actualDirEntry = null;
            }

            if ($file->isDir()) {
                // Получение записи каталога с таким именем из БД
                // Если нет, тогда создаем её
                if (is_null($actualDirEntry)) {
                    // Создание директории
                    $actualDirEntry = $this->saveDirectory($file, $pathName, $rootDirEntry);
                }
                // Получаем её и даем в качестве корня на следующий шаг.
                $this->scanDirectory($actualDirEntry, $this->toUtf($file->getRealPath()), $level + 1);

            } else {
                if (is_null($actualDirEntry)) {
                    // Создание файла
                    $this->saveFile($file, $pathName, $rootDirEntry);
                } else {
                    // Проверка наличия изменений в файле
                    $this->checkUpdateFile($file, $actualDirEntry);
                }
            }
        }

        // Если корневая директория существует
        if (!is_null($rootDirEntry->getId())) {
            // Если у нее есть потомки
            if (!is_null($childDirEntries) and count($childDirEntries) > 0) {
                // Тогда удаляем все записи, о файлах которые не встретились во время обхода каталога
                $query = $this->entityManager->createQuery(
                    'DELETE FROM ExplorerBundle:DirEntry p
                      WHERE p IN (:entries)')
                    ->setParameter('entries', $childDirEntries);
                $query->execute();
            }

        }

    }


}