var rootDirEntryId = null;
var repositoryInfo = null;
var currentDirEntryId = null;
var dirEntries = null;

const DIR_ENTRY_TYPE_DIRECTORY = 0;

function build_tree(dirs, parent_id) {
    var foundDirs = $.grep(dirs, function (d) {
        return d.dirEntryId == parent_id;
    });
    var result = '';
    if (foundDirs.length > 0) {
        result += '<ul>';
        $(foundDirs).each(function (i, dir) {
            var tree = build_tree(dirs, dir.id);
            if (tree.length == 0) {
                result += '<li><span><span class="glyphicon glyphicon-folder-close"></span><a href="#' + dir.id + '" title="' + dir.name + '">' + dir.name + '</a> </span></li>';
            }
            else {
                result += '<li class="parent_li"><span><span class="glyphicon glyphicon-folder-open"></span><a href="#' + dir.id + '" title="' + dir.name + '">' + dir.name + '</a> </span></li>';
            }
            result += tree;
            result += '</li>';
        });
        result += '</ul>';
    }
    return result;
}
function getParentId(childDirEntryId) {
    var dirEntry = $.grep(dirEntries, function (d) {
        return d.id == childDirEntryId;
    })[0];
    return typeof(dirEntry) === 'undefined' ? '' : dirEntry.dirEntryId;

}

function resizeLayers() {
    var height = "innerHeight" in window
        ? window.innerHeight
        : document.documentElement.offsetHeight;
    console.log('h');
    $('.easy-tree').height(height - 58);
    $('.file-browser > .layer').height(height - $('.status-bar').height() - 55);
}

function formatNum(num) {
    return '<nobr>' + (num).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + '</nobr>';

}

function fillTableBody(data) {
    var length = data.length;
    var filesCount = 0;
    $("#file-browser-table-body").html('');
    if (currentDirEntryId != rootDirEntryId) {
        $("#file-browser-table-body").append($("<tr>").attr("colspan", "5").attr("class", "root").append('<a href="#' + getParentId(currentDirEntryId) + '">..</a>'));
    }

    $(data).each(function (i, el) {
        var gluph = '<span class="glyphicon glyphicon-' + (el.type == 0 ? 'folder-close' : 'file') + '"></span>';
        var name = el.type == DIR_ENTRY_TYPE_DIRECTORY ? '<a href="#' + el.id + '">' + gluph + el.name + '</a>' : gluph + el.name;
        var row = '<td>' + name + '</td>';
        filesCount += el.type == DIR_ENTRY_TYPE_DIRECTORY ? 0 : 1;
        row += '<td>' + (el.type == DIR_ENTRY_TYPE_DIRECTORY ? 'folder' : el.mimeType) + '</td>';
        row += '<td>' + (el.type == DIR_ENTRY_TYPE_DIRECTORY ? '' : formatNum(el.size)) + '</td>';
        row += '<td>' + el.created + '</td>';
        row += '<td>' + el.modified + '</td>';

        $("#file-browser-table-body").append($("<tr>")).append(row);
    });
    $('#files-in-dir').html(formatNum(filesCount));
    $('#dirs-in-dir').html(formatNum(length - filesCount));
}

function getFolderContents(dirEntryId) {
    currentDirEntryId = dirEntryId;
    $.ajax({
        url: "/api/getFolderContents/" + dirEntryId,
        dataType: 'json'
    }).done(function (data) {
        fillTableBody(data)
    });
}

function createDirectoryTree(data) {
    dirEntries = data;
    var html = '<div class="easy-tree"><ul>';
    html += '<li class="parent_li"><span><span class="glyphicon glyphicon-folder-open"></span><a href="#' + rootDirEntryId + '" title="root">root</a> </span></li>';
    html += build_tree(data, rootDirEntryId);
    html += '</ul></div>';
    $('.directory-tree').html(html);
}

$(window).on('resize', function () {
    resizeLayers()
});

$(document).ready(function () {
    $.ajax({
        url: "/api/info",
        dataType: 'json'
    }).done(function (data) {
        repositoryInfo = data;
        rootDirEntryId = data.root.id;
        $('#files-in-repo').html(formatNum(repositoryInfo.files));
        $('#dirs-in-repo').html(formatNum(repositoryInfo.folders));
        $('#sizeof-repo').html(formatNum(repositoryInfo.size));

        $.ajax({
            url: "/api/folders",
            dataType: 'json'
        }).done(function (data) {
            createDirectoryTree(data);

            if (window.location.hash.length > 0) {
                var dirEntryId = $(window.location.hash.split('#')).last()[0];
                getFolderContents(dirEntryId);
            }

            $('.easy-tree, .file-browser').on('click', 'a', function (e) {
                if (e.currentTarget === e.target) {
                    getFolderContents($($(e.target).attr('href').split('#')).last()[0])
                }
            });

            resizeLayers();
        });
    });

});