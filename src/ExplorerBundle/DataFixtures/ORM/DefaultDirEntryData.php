<?php
namespace ExplorerBundle\DataFixture\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ExplorerBundle\Entity\DirEntry;
use ExplorerBundle\Entity\EntryDir;

class DefaultDirEntryData implements FixtureInterface
{

    /**
     * Создает корневой элемент репозитория, который будет родителем у первого уровня.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $dir = new DirEntry();
        $dir->setName('|$ROOT$|');
        $dir->setSize(0);

        $manager->persist($dir);
        $manager->flush();
    }
}