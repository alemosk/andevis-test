<?php

namespace ExplorerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class EntryFile
 * @package ExplorerBundle\Entity
 * @ORM\Table(name="dir_entry")
 * @ORM\Entity
 */
class DirEntry
{
    const TYPE_DIRECTORY = 0;
    const TYPE_FILE = 1;
    // Недопустимое для ФС имя файла. В БД используется как корневой каталог.
    const ROOT_SIGNATURE = '|$ROOT$|';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $size;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mime_type;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @ORM\ManyToOne(targetEntity="DirEntry", inversedBy="files")
     * @ORM\JoinColumn(name="dir_entry_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $directory;

    /**
     * @ORM\OneToMany(targetEntity="DirEntry", mappedBy="directory")
     */
    private $files;

    /**
     * Конвертирует DirEntry объект в массив.
     *
     * Стандартные PHP сериализаторы не работаю с объектами DirEntry.
     * @param $dirEntry
     * @return array
     */
    public static function convertToArray($dirEntry)
    {
        return array(
            'id' => $dirEntry->getId(),
            'dirEntryId' => is_null($dirEntry->getDirectory()) ? null : $dirEntry->getDirectory()->getId(),
            'size' => $dirEntry->getSize(),
            'type' => $dirEntry->getType(),
            'mimeType' => $dirEntry->getMimeType(),
            'name' => $dirEntry->getName(),
            'created' => is_null($dirEntry->getCreated()) ? null : $dirEntry->getCreated()->format('Y-m-d H:i:s'),
            'modified' => is_null($dirEntry->getModified()) ? null : $dirEntry->getModified()->format('Y-m-d H:i:s')
        );

    }

    /**
     * Конвертирует массив объектов DirEntry в массив массивов DirEntry.
     * @param $dirEntryList
     * @return mixed
     */
    public static function objectToArrayList($dirEntryList)
    {
        for ($i = 0; $i < count($dirEntryList); $i++) {
            $dirEntryList[$i] = self::convertToArray($dirEntryList[$i]);
        }
        return $dirEntryList;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return DirEntry
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return DirEntry
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set mime_type
     *
     * @param string $mimeType
     * @return DirEntry
     */
    public function setMimeType($mimeType)
    {
        $this->mime_type = $mimeType;

        return $this;
    }

    /**
     * Get mime_type
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mime_type;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return DirEntry
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set directory
     *
     * @param \ExplorerBundle\Entity\DirEntry $directory
     * @return DirEntry
     */
    public function setDirectory(\ExplorerBundle\Entity\DirEntry $directory = null)
    {
        $this->directory = $directory;

        return $this;
    }

    /**
     * Get directory
     *
     * @return \ExplorerBundle\Entity\DirEntry
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * Add files
     *
     * @param \ExplorerBundle\Entity\DirEntry $files
     * @return DirEntry
     */
    public function addFile(\ExplorerBundle\Entity\DirEntry $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \ExplorerBundle\Entity\DirEntry $files
     */
    public function removeFile(\ExplorerBundle\Entity\DirEntry $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set dirEntryId
     *
     * @param integer $dirEntryId
     * @return DirEntry
     */
    public function setDirEntryId($dirEntryId)
    {
        $this->dirEntryId = $dirEntryId;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return DirEntry
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modyfied
     *
     * @param \DateTime $modified
     * @return DirEntry
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modyfied
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }
}
