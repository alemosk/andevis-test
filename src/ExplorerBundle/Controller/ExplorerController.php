<?php
namespace ExplorerBundle\Controller;

use ExplorerBundle\Entity\DirEntry;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;


class ExplorerController extends Controller
{
    /**
     * Отображает главная страницу
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('@ExplorerBundle/Resources/views/index.html.twig');
    }

    /**
     * Возвращает количество записей о каталогах в БД
     * @param $type
     * @return mixed
     */
    private function getDirItemsCount($type)
    {
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb->select('count(d.id)');
        $qb->from('ExplorerBundle:DirEntry', 'd');
        $qb->where('d.type = ?1');
        $qb->setParameters(array(1 => $type));

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Возвращает размер всех файлов о которых имеются записи в БД
     * @return mixed
     */
    private function getFilesSize()
    {
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb->select('sum(d.size)');
        $qb->from('ExplorerBundle:DirEntry', 'd');
        $qb->where('d.type = ?1');
        $qb->setParameters(array(1 => DirEntry::TYPE_FILE));

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Возвращает JSON объект с общей информацией о репозитории: кол-во файлов, размер, корневой элемент и т.д.
     * @return JsonResponse
     */
    public function getInfoAction()
    {
        $repository = $this->getDoctrine()->getRepository('ExplorerBundle:DirEntry');
        $dirEntry = $repository->findOneByName(DirEntry::ROOT_SIGNATURE);
        // Количество папок
        $countFolders = $this->getDirItemsCount(DirEntry::TYPE_DIRECTORY);
        // Количество файлов
        $countFiles = $this->getDirItemsCount(DirEntry::TYPE_FILE);
        // Общий размер
        $filesSize = $this->getFilesSize();
        $response = array(
            'root' => DirEntry::convertToArray($dirEntry),
            'folders' => $countFolders,
            'files' => $countFiles,
            'size' => $filesSize
        );

        return new JsonResponse($response);
    }

    /**
     * Возвращает JSON объект с дочерними элементами каталога, ID которого был указан.
     * @param $dirEntryId
     * @return JsonResponse
     */
    public function getFolderContentsAction($dirEntryId)
    {
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();

        $qb->select('d')
            ->from('ExplorerBundle:DirEntry', 'd')
            ->where('d.directory = ?1')
            ->addOrderBy('d.type', 'ASC')
            ->addOrderBy('d.name', 'ASC')
            ->setParameters(array(1 => intval($dirEntryId)));

        $dirEntries = $qb->getQuery()->getResult();

        return new JsonResponse(DirEntry::objectToArrayList($dirEntries));

    }

    /**
     * Возвращает список всех каталогов в репозитории (для постороения дерева)
     * @return JsonResponse
     */
    public function getFoldersAction()
    {
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();

        $qb->select('d')
            ->from('ExplorerBundle:DirEntry', 'd')
            ->where('d.type = ?1')
            ->addOrderBy('d.name', 'ASC')
            ->setParameters(array(1 => DirEntry::TYPE_DIRECTORY));

        $directories = $qb->getQuery()->getResult();

        $directories = DirEntry::objectToArrayList($directories);

        return new JsonResponse($directories);
    }
}