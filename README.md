#Explorer - тестовое задание для Andevis AS.

##Установка и настройка

### Предварительный этап
Для установки и запуска проекта в системе должны быть установлены [PHP](http://php.net "PHP"),
[Composer](https://getcomposer.org/ "Composer"), [MySQL](https://mysql.com/ "Oracle MySQL") и [Git](https://git-scm.com/, "Git").
Также они должны быть доступны в переменной окружения PATH.


Загрузить проект [Explorer](https://bitbucket.org/alemosk/andevis-test "Explorer") в систему можно следующим образом.
```git 
$ git clone git@bitbucket.org:alemosk/andevis-test.git
$ cd andevis-test
```

### Создание БД
Сначала нужно создать MySQL базу данных для проекта.
```
$ mysql -u root
```
В строке приглашения MySQL
```
MySQL [(none)]>
```
необходимо выполнить следующий SQL запрос, который создаст БД. 

Имя БД `andevis_test` можно заменить на другое.  
```mysql
CREATE DATABASE `andevis_test` CHARACTER SET utf8 COLLATE utf8_general_ci;
```
После того как запрос завершился из MySQL можно выйти командой
```
MySQL [(none)]> \q
```


### Установка зависимостей
Необходимо перейти в каталог с проектом.
После того как БД создана необходимо установить зависимости.
```
$ php composer.phar install
```

В процессе установки будут запрошены настройки БД
```
Some parameters are missing. Please provide them.
database_host (127.0.0.1):
database_port (null):
database_name (symfony): andevis_test
database_user (root):
database_password (null):
mailer_transport (smtp):
mailer_host (127.0.0.1):
mailer_user (null):
mailer_password (null):
secret (ThisTokenIsNotSoSecretChangeIt):
```

### Создание таблиц в БД
После завершения установки зависимостей необходимо создать таблицы в БД.

```
$ php app/console doctrine:schema:update --force
```

### Создание служебных записей в БД
Установка служебных данных необходимых для корректной работы.
```
$ php app/console doctrine:fixtures:load
```
На вопрос-предупреждение об удалении всех данных в БД, нужно ответить утвердительно. 
```
Careful, database will be purged. Do you want to continue y/N ? y
```

### Настройка статических файлов
Чтобы сайт видел статические фалы выполняем следующею команду
```
$ php app/console assetic:dump
```

## Использование Explorer
После выполнения всех шагов выше, можно начинать работу с проектом.

Для синхронизации содержимого каталога с БД используется следующая команда
```
$ php app/console explorer:sync /c/freemind/doc
```

## Запуск встроенного сервера
Если Explorer не находится в папке веб-сервера, тогда его можно запустить на встроенном веб сервере.
```
$ php app/console server:start
```
Теперь можно открыть страницу [http://127.0.0.1:8000](http://127.0.0.1:8000) и наблюдать результат.

Для остановки сервера надо нажать CTRL+C